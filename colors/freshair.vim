" ===============================================================
" freshair
" 
" URL: https://gitlab.com/boweevil/freshair.git
" Author: Jason Carpenter (boweevil)
" License: MIT
" Last Change: 2020/03/03 17:45
" ===============================================================

set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="freshair"


let Italic = ""
if exists('g:freshair_italic')
  let Italic = "italic"
endif
let g:freshair_italic = get(g:, 'freshair_italic', 0)

let Bold = ""
if exists('g:freshair_bold')
  let Bold = "bold"
endif

let g:freshair_bold = get(g:, 'freshair_bold', 0)
hi link ALEError SpellBad
hi link ALEErrorLine SpellBad
hi ALEErrorSign guifg=#D32F3F ctermfg=161 guibg=#D7CCC8 ctermbg=252 gui=Bold cterm=Bold
hi link ALEInfo ALEWarning
hi ALEInfoSign guifg=#00838F ctermfg=30 guibg=#D7CCC8 ctermbg=252 gui=Bold cterm=Bold
hi link ALEStyleError ALEError
hi link ALEStyleErrorSign ALEErrorSign
hi link ALEStyleWarning ALEError
hi link ALEStyleWarningSign ALEWarningSign
hi link ALEWarning SpellCap
hi ALEWarningSign guifg=#F57C00 ctermfg=208 guibg=#D7CCC8 ctermbg=252 gui=Bold cterm=Bold
hi ColorColumn guifg=#757575 ctermfg=243 guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi Conceal guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorColumn guifg=NONE ctermfg=NONE guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi CursorLine guifg=NONE ctermfg=NONE guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi CursorLineNr guifg=#F57C00 ctermfg=208 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi Directory guifg=#01579B ctermfg=24 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffAdd guifg=#E0DCD1 ctermfg=253 guibg=#33691E ctermbg=236 gui=NONE cterm=NONE
hi DiffChange guifg=#E0DCD1 ctermfg=253 guibg=#00695C ctermbg=23 gui=NONE cterm=NONE
hi DiffDelete guifg=#E0DCD1 ctermfg=253 guibg=#B71C1C ctermbg=124 gui=NONE cterm=NONE
hi DiffText guifg=#E0DCD1 ctermfg=253 guibg=#E65100 ctermbg=166 gui=NONE cterm=NONE
hi ErrorMsg guifg=#B71C1C ctermfg=124 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi VertSplit guifg=#E0DCD1 ctermfg=253 guibg=#8D6E63 ctermbg=95 gui=NONE cterm=NONE
hi Folded guifg=#757575 ctermfg=243 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi FoldColumn guifg=#757575 ctermfg=243 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi SignColumn guifg=#757575 ctermfg=243 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi IncSearch guifg=#F5F5F5 ctermfg=255 guibg=#E65100 ctermbg=166 gui=Bold,underline cterm=Bold,underline
hi LineNr guifg=#757575 ctermfg=243 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi MatchParen guifg=#512DA8 ctermfg=55 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi ModeMsg guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi MoreMsg guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NonText guifg=#9E9E9E ctermfg=247 guibg=NONE ctermbg=NONE gui=Italic cterm=Italic
hi Normal guifg=#424242 ctermfg=238 guibg=#F5F5F5 ctermbg=255 gui=NONE cterm=NONE
hi PMenu guifg=#424242 ctermfg=238 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi PMenuSel guifg=#E0DCD1 ctermfg=253 guibg=#00695C ctermbg=23 gui=Bold cterm=Bold
hi PmenuSbar guifg=NONE ctermfg=NONE guibg=#757575 ctermbg=243 gui=NONE cterm=NONE
hi PmenuThumb guifg=NONE ctermfg=NONE guibg=#E65100 ctermbg=166 gui=NONE cterm=NONE
hi Question guifg=#D7CCC8 ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Search guifg=#F5F5F5 ctermfg=255 guibg=#E65100 ctermbg=166 gui=Bold cterm=Bold
hi SpecialKey guifg=#512DA8 ctermfg=55 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellBad guifg=#B71C1C ctermfg=124 guibg=NONE ctermbg=NONE gui=undercurl cterm=undercurl
hi SpellLocal guifg=#00695C ctermfg=23 guibg=NONE ctermbg=NONE gui=undercurl cterm=undercurl
hi SpellCap guifg=#B71C1C ctermfg=124 guibg=NONE ctermbg=NONE gui=undercurl cterm=undercurl
hi SpellRare guifg=#512DA8 ctermfg=55 guibg=NONE ctermbg=NONE gui=undercurl cterm=undercurl
hi StatusLine guifg=#795548 ctermfg=95 guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi StatusLineNC guifg=#A1887F ctermfg=138 guibg=#EFEBE9 ctermbg=255 gui=NONE cterm=NONE
hi TabLine guifg=#757575 ctermfg=243 guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi TabLineFill guifg=#757575 ctermfg=243 guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi TabLineSel guifg=#F57C00 ctermfg=208 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi Title guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Visual guifg=#E0DCD1 ctermfg=253 guibg=#E65100 ctermbg=166 gui=NONE cterm=NONE
hi WarningMsg guifg=#E0DCD1 ctermfg=253 guibg=#E65100 ctermbg=166 gui=Bold cterm=Bold
hi WildMenu guifg=#F57C00 ctermfg=208 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi User1 guifg=#F57C00 ctermfg=208 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi User2 guifg=#D32F3F ctermfg=161 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi Comment guifg=#9E9E9E ctermfg=247 guibg=NONE ctermbg=NONE gui=Italic cterm=Italic
hi Constant guifg=#00695C ctermfg=23 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link String Constant
hi link Character Constant
hi link Boolean Constant
hi link Number Constant
hi link Float Constant
hi Identifier guifg=#512DA8 ctermfg=55 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link Function Identifier
hi Statement guifg=#33691E ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link Conditional Statement
hi link Repeat Statement
hi link Label Statement
hi link Operator Statement
hi link Keyword Statement
hi link Exception Statement
hi PreProc guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link Include PreProc
hi link Define PreProc
hi link Macro PreProc
hi link PreCondit PreProc
hi Type guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link StorageClass Type
hi link Structure Type
hi link Typedef Type
hi Special guifg=#B71C1C ctermfg=124 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link SpecialChar Special
hi link Tag Special
hi link Delimiter Special
hi link SpecialComment Special
hi link Debug Special
hi Underlined guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi Error guifg=#F5F5F5 ctermfg=255 guibg=#B71C1C ctermbg=124 gui=Bold,Italic cterm=Bold,Italic
hi Todo guifg=#F5F5F5 ctermfg=255 guibg=#512DA8 ctermbg=55 gui=Bold,Italic cterm=Bold,Italic
hi bufExplorerHelp guifg=#757575 ctermfg=243 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link bufExplorerTitle Comment
hi bufExplorerMapping guifg=#7B1FA2 ctermfg=91 guibg=NONE ctermbg=NONE gui=Bold,Italic cterm=Bold,Italic
hi bufExplorerSortBy guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=Italic cterm=Italic
hi bufExplorerCurBuf guifg=#33691E ctermfg=236 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi bufExplorerAltBuf guifg=#00695C ctermfg=23 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi bufExplorerHidBuf guifg=#01579B ctermfg=24 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link cssComment Comment
hi link cssVendor Comment
hi link cssHacks Comment
hi link cssTagName Statement
hi link cssDeprecated Error
hi link cssSelectorOp Special
hi link cssSelectorOp2 Special
hi link cssAttrComma Special
hi link cssAnimationProp cssProp
hi link cssBackgroundProp cssProp
hi link cssBorderProp cssProp
hi link cssBoxProp cssProp
hi link cssColorProp cssProp
hi link cssContentForPagedMediaProp cssProp
hi link cssDimensionProp cssProp
hi link cssFlexibleBoxProp cssProp
hi link cssFontProp cssProp
hi link cssGeneratedContentProp cssProp
hi link cssGridProp cssProp
hi link cssHyerlinkProp cssProp
hi link cssLineboxProp cssProp
hi link cssListProp cssProp
hi link cssMarqueeProp cssProp
hi link cssMultiColumnProp cssProp
hi link cssPagedMediaProp cssProp
hi link cssPositioningProp cssProp
hi link cssPrintProp cssProp
hi link cssRubyProp cssProp
hi link cssSpeechProp cssProp
hi link cssTableProp cssProp
hi link cssTextProp cssProp
hi link cssTransformProp cssProp
hi link cssTransitionProp cssProp
hi link cssUIProp cssProp
hi link cssIEUIProp cssProp
hi link cssAuralProp cssProp
hi link cssRenderProp cssProp
hi link cssMobileTextProp cssProp
hi link cssAnimationAttr cssAttr
hi link cssBackgroundAttr cssAttr
hi link cssBorderAttr cssAttr
hi link cssBoxAttr cssAttr
hi link cssContentForPagedMediaAttr cssAttr
hi link cssDimensionAttr cssAttr
hi link cssFlexibleBoxAttr cssAttr
hi link cssFontAttr cssAttr
hi link cssGeneratedContentAttr cssAttr
hi link cssGridAttr cssAttr
hi link cssHyerlinkAttr cssAttr
hi link cssLineboxAttr cssAttr
hi link cssListAttr cssAttr
hi link cssMarginAttr cssAttr
hi link cssMarqueeAttr cssAttr
hi link cssMultiColumnAttr cssAttr
hi link cssPaddingAttr cssAttr
hi link cssPagedMediaAttr cssAttr
hi link cssPositioningAttr cssAttr
hi link cssGradientAttr cssAttr
hi link cssPrintAttr cssAttr
hi link cssRubyAttr cssAttr
hi link cssSpeechAttr cssAttr
hi link cssTableAttr cssAttr
hi link cssTextAttr cssAttr
hi link cssTransformAttr cssAttr
hi link cssTransitionAttr cssAttr
hi link cssUIAttr cssAttr
hi link cssIEUIAttr cssAttr
hi link cssAuralAttr cssAttr
hi link cssRenderAttr cssAttr
hi link cssCommonAttr cssAttr
hi link cssPseudoClassId PreProc
hi link cssPseudoClassLang Constant
hi link cssValueLength Number
hi link cssValueInteger Number
hi link cssValueNumber Number
hi link cssValueAngle Number
hi link cssValueTime Number
hi link cssValueFrequency Number
hi link cssFunction Constant
hi link cssURL String
hi link cssFunctionName Function
hi link cssFunctionComma Function
hi link cssColor Constant
hi link cssIdentifier Function
hi link cssInclude Include
hi link cssIncludeKeyword atKeyword
hi link cssImportant Special
hi link cssBraces Function
hi link cssBraceError Error
hi link cssError Error
hi link cssUnicodeEscape Special
hi link cssStringQQ String
hi link cssStringQ String
hi link cssAttributeSelector String
hi link cssMedia atKeyword
hi link cssMediaType Special
hi link cssMediaComma Normal
hi link cssMediaKeyword Statement
hi link cssMediaProp cssProp
hi link cssMediaAttr cssAttr
hi link cssPage atKeyword
hi link cssPagePseudo PreProc
hi link cssPageMargin atKeyword
hi link cssPageProp cssProp
hi link cssKeyFrame atKeyword
hi link cssKeyFrameSelector Constant
hi link cssFontDescriptor Special
hi link cssFontDescriptorFunction Constant
hi link cssFontDescriptorProp cssProp
hi link cssFontDescriptorAttr cssAttr
hi link cssUnicodeRange Constant
hi link cssClassName Function
hi link cssClassNameDot Function
hi link cssProp StorageClass
hi link cssAttr Constant
hi link cssUnitDecorators Number
hi link cssNoise Noise
hi link atKeyword PreProc
hi link CtrlPNoEntries Error
hi CtrlPMatch guifg=#33691E ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CtrlPLinePre guifg=#F5F5F5 ctermfg=255 guibg=#757575 ctermbg=243 gui=reverse cterm=reverse
hi link CtrlPPrtBase Comment
hi link CtrlPPrtText Normal
hi link CtrlPPrtCursor Constant
hi link CtrlPTabExtra Comment
hi link CtrlPBufName Directory
hi link CtrlPTagKind Title
hi link CtrlPqfLineCol Comment
hi link CtrlPUndoT Directory
hi link CtrlPUndoBr Comment
hi link CtrlPUndoNr String
hi link CtrlPUndoSv Comment
hi link CtrlPUndoPo Title
hi link CtrlPBookmark Identifier
hi CtrlPMode1 guifg=#00838F ctermfg=30 guibg=#E0DCD1 ctermbg=253 gui=Bold cterm=Bold
hi CtrlPMode2 guifg=#757575 ctermfg=243 guibg=#E0DCD1 ctermbg=253 gui=NONE cterm=NONE
hi CtrlPStats guifg=#D32F3F ctermfg=161 guibg=NONE ctermbg=NONE gui=Bold,Italic cterm=Bold,Italic
hi link diffOldFile diffFile
hi link diffNewFile diffFile
hi link diffFile Type
hi link diffOnly Constant
hi link diffIdentical Constant
hi link diffDiffer Constant
hi link diffBDiffer Constant
hi link diffIsA Constant
hi link diffNoEOL Constant
hi link diffCommon Constant
hi link diffRemoved Special
hi link diffChanged PreProc
hi link diffAdded Identifier
hi link diffLine Statement
hi link diffSubname PreProc
hi link diffComment Comment
hi link FugitiveblameBoundary Keyword
hi link FugitiveblameHash Identifier
hi link FugitiveblameUncommitted Ignore
hi link FugitiveblameTime PreProc
hi link FugitiveblameLineNumber Number
hi link FugitiveblameOriginalFile String
hi link FugitiveblameShort FugitiveblameDelimiter
hi link FugitiveblameDelimiter Delimiter
hi link FugitiveblameNotCommittedYet Comment
hi link gitDateHeader gitIdentityHeader
hi link gitIdentityHeader gitIdentityKeyword
hi link gitIdentityKeyword Label
hi link gitNotesHeader gitKeyword
hi link gitReflogHeader gitKeyword
hi link gitKeyword Keyword
hi link gitIdentity String
hi link gitEmailDelimiter Delimiter
hi link gitEmail Special
hi link gitDate Number
hi link gitMode Number
hi link gitHashAbbrev gitHash
hi link gitHash Identifier
hi link gitReflogMiddle gitReference
hi link gitReference Function
hi link gitStage gitType
hi link gitType Type
hi link gitDiffAdded diffAdded
hi link gitDiffRemoved diffRemoved
hi link gitcommitSummary Keyword
hi link gitcommitComment Comment
hi link gitcommitUntracked gitcommitComment
hi link gitcommitDiscarded gitcommitComment
hi link gitcommitSelected gitcommitComment
hi link gitcommitUnmerged gitcommitComment
hi link gitcommitOnBranch Comment
hi link gitcommitBranch Special
hi link gitcommitNoBranch gitCommitBranch
hi link gitcommitDiscardedType gitcommitType
hi link gitcommitSelectedType gitcommitType
hi link gitcommitUnmergedType gitcommitType
hi link gitcommitType Type
hi link gitcommitNoChanges gitcommitHeader
hi link gitcommitHeader PreProc
hi link gitcommitUntrackedFile gitcommitFile
hi link gitcommitDiscardedFile gitcommitFile
hi link gitcommitSelectedFile gitcommitFile
hi link gitcommitUnmergedFile gitcommitFile
hi link gitcommitFile Constant
hi link gitcommitDiscardedArrow gitcommitArrow
hi link gitcommitSelectedArrow gitcommitArrow
hi link gitcommitUnmergedArrow gitcommitArrow
hi link gitcommitArrow gitcommitComment
hi link gitcommitBlank Error
hi link gitconfigComment Comment
hi link gitconfigSection Keyword
hi link gitconfigVariable Identifier
hi link gitconfigBoolean Boolean
hi link gitconfigNumber Number
hi link gitconfigString String
hi link gitconfigDelim Delimiter
hi link gitconfigEscape Delimiter
hi link gitconfigError Error
hi link gitrebaseCommit gitrebaseHash
hi link gitrebaseHash Identifier
hi link gitrebasePick Statement
hi link gitrebaseReword Number
hi link gitrebaseEdit PreProc
hi link gitrebaseSquash Type
hi link gitrebaseFixup Special
hi link gitrebaseExec Function
hi link gitrebaseSummary String
hi link gitrebaseComment Comment
hi link gitrebaseSquashError Error
hi link goDirective Statement
hi link goDeclaration Keyword
hi link goDeclType Keyword
hi link goStatement Statement
hi link goConditional Conditional
hi link goLabel Label
hi link goRepeat Repeat
hi link goType Type
hi link goSignedInts Type
hi link goUnsignedInts Type
hi link goFloats Type
hi link goComplexes Type
hi link goBuiltins Keyword
hi link goConstants Keyword
hi link goComment Comment
hi link goTodo Todo
hi link goEscapeOctal goSpecialString
hi link goEscapeC goSpecialString
hi link goEscapeX goSpecialString
hi link goEscapeU goSpecialString
hi link goEscapeBigU goSpecialString
hi link goSpecialString Special
hi link goEscapeError Error
hi link goString String
hi link goRawString String
hi link goCharacter Character
hi link goDecimalInt Integer
hi link goHexadecimalInt Integer
hi link goOctalInt Integer
hi link Integer Number
hi link goFloat Float
hi link goImaginary Number
hi link goExtraType Type
hi link goSpaceError Error
hi link helpIgnore Ignore
hi link helpHyperTextJump Identifier
hi link helpBar Ignore
hi link helpBacktick Ignore
hi link helpStar Ignore
hi link helpHyperTextEntry String
hi link helpHeadline Statement
hi link helpHeader PreProc
hi link helpSectionDelim PreProc
hi link helpVim Identifier
hi link helpCommand Comment
hi link helpExample Comment
hi link helpOption Type
hi link helpSpecial Special
hi link helpNote Todo
hi link helpComment Comment
hi link helpConstant Constant
hi link helpString String
hi link helpCharacter Character
hi link helpNumber Number
hi link helpBoolean Boolean
hi link helpFloat Float
hi link helpIdentifier Identifier
hi link helpFunction Function
hi link helpStatement Statement
hi link helpConditional Conditional
hi link helpRepeat Repeat
hi link helpLabel Label
hi link helpOperator Operator
hi link helpKeyword Keyword
hi link helpException Exception
hi link helpPreProc PreProc
hi link helpInclude Include
hi link helpDefine Define
hi link helpMacro Macro
hi link helpPreCondit PreCondit
hi link helpType Type
hi link helpStorageClass StorageClass
hi link helpStructure Structure
hi link helpTypedef Typedef
hi link helpSpecialChar SpecialChar
hi link helpTag Tag
hi link helpDelimiter Delimiter
hi link helpSpecialComment SpecialComment
hi link helpDebug Debug
hi link helpUnderlined Underlined
hi link helpError Error
hi link helpTodo Todo
hi link helpURL String
hi link htmlTag Function
hi link htmlEndTag Identifier
hi link htmlArg Type
hi link htmlTagName htmlStatement
hi link htmlSpecialTagName Exception
hi link htmlValue String
hi link htmlH1 Title
hi link htmlH2 htmlH1
hi link htmlH3 htmlH2
hi link htmlH4 htmlH3
hi link htmlH5 htmlH4
hi link htmlH6 htmlH5
hi link htmlHead PreProc
hi link htmlTitle Title
hi link htmlBoldItalicUnderline htmlBoldUnderlineItalic
hi link htmlUnderlineBold htmlBoldUnderline
hi link htmlUnderlineItalicBold htmlBoldUnderlineItalic
hi link htmlUnderlineBoldItalic htmlBoldUnderlineItalic
hi link htmlItalicUnderline htmlUnderlineItalic
hi link htmlItalicBold htmlBoldItalic
hi link htmlItalicBoldUnderline htmlBoldUnderlineItalic
hi link htmlItalicUnderlineBold htmlBoldUnderlineItalic
hi link htmlLink Underlined
hi link htmlLeadingSpace None
hi link htmlPreStmt PreProc
hi link htmlPreError Error
hi link htmlPreProc PreProc
hi link htmlPreAttr String
hi link htmlPreProcAttrName PreProc
hi link htmlPreProcAttrError Error
hi link htmlSpecial Special
hi link htmlSpecialChar Special
hi link htmlString String
hi link htmlStatement Statement
hi link htmlComment Comment
hi link htmlCommentPart Comment
hi link htmlCommentError htmlError
hi link htmlTagError htmlError
hi link htmlEvent javaScript
hi link htmlError Error
hi link javaScript Special
hi link javaScriptExpression javaScript
hi link htmlCssStyleComment Comment
hi link htmlCssDefinition Special
hi link javaScriptComment Comment
hi link javaScriptLineComment Comment
hi link javaScriptCommentTodo Todo
hi link javaScriptSpecial Special
hi link javaScriptStringS String
hi link javaScriptStringD String
hi link javaScriptCharacter Character
hi link javaScriptSpecialCharacter javaScriptSpecial
hi link javaScriptNumber javaScriptValue
hi link javaScriptConditional Conditional
hi link javaScriptRepeat Repeat
hi link javaScriptBranch Conditional
hi link javaScriptOperator Operator
hi link javaScriptType Type
hi link javaScriptStatement Statement
hi link javaScriptFunction Function
hi link javaScriptBraces Function
hi link javaScriptError Error
hi link javaScriptParensError Error
hi link javaScriptNull Keyword
hi link javaScriptBoolean Boolean
hi link javaScriptRegexpString String
hi link javaScriptIdentifier Identifier
hi link javaScriptLabel Label
hi link javaScriptException Exception
hi link javaScriptMessage Keyword
hi link javaScriptGlobal Keyword
hi link javaScriptMember Keyword
hi link javaScriptDeprecated Exception
hi link javaScriptReserved Keyword
hi link javaScriptDebug Debug
hi link javaScriptConstant Label
hi link jsonPadding Operator
hi link jsonString String
hi link jsonTest Label
hi link jsonEscape Special
hi link jsonNumber Number
hi link jsonBraces Delimiter
hi link jsonNull Function
hi link jsonBoolean Boolean
hi link jsonKeyword Label
hi link jsonNumError Error
hi link jsonCommentError Error
hi link jsonSemicolonError Error
hi link jsonTrailingCommaError Error
hi link jsonMissingCommaError Error
hi link jsonStringSQError Error
hi link jsonNoQuotesError Error
hi link jsonTripleQuotesError Error
hi link jsonQuote Quote
hi link jsonNoise Noise
hi link lessEndOfLineComment lessComment
hi link lessCssComment lessComment
hi link lessComment Comment
hi link lessDefault cssImportant
hi link lessVariable Identifier
hi link lessFunction PreProc
hi link lessTodo Todo
hi link lessInclude Include
hi link lessIdChar Special
hi link lessClassChar Special
hi link lessAmpersand Character
hi link lessId Identifier
hi link lessClass Type
hi link lessCssAttribute PreProc
hi link lessClassCall Type
hi link lessClassIdCall Type
hi link lessTagName cssTagName
hi link lessDeprecated cssDeprecated
hi link lessMedia cssMedia
hi link markdownH1 htmlH1
hi link markdownH2 htmlH2
hi link markdownH3 htmlH3
hi link markdownH4 htmlH4
hi link markdownH5 htmlH5
hi link markdownH6 htmlH6
hi link markdownHeadingRule markdownRule
hi link markdownHeadingDelimiter Delimiter
hi link markdownOrderedListMarker markdownListMarker
hi link markdownListMarker htmlTagName
hi link markdownBlockquote Comment
hi link markdownRule PreProc
hi link markdownLinkText htmlLink
hi link markdownIdDeclaration Typedef
hi link markdownId Type
hi link markdownAutomaticLink markdownUrl
hi link markdownUrl Float
hi link markdownUrlTitle String
hi link markdownIdDelimiter markdownLinkDelimiter
hi link markdownUrlDelimiter htmlTag
hi link markdownUrlTitleDelimiter Delimiter
hi link markdownItalic htmlItalic
hi link markdownBold htmlBold
hi link markdownBoldItalic htmlBoldItalic
hi link markdownCodeDelimiter Delimiter
hi link markdownEscape Special
hi link markdownError Error
hi link NERDTreePart Special
hi link NERDTreePartFile Type
hi link NERDTreeExecFile Title
hi link NERDTreeDirSlash Identifier
hi link NERDTreeBookmarksHeader statement
hi link NERDTreeBookmarksLeader ignore
hi link NERDTreeBookmarkName Identifier
hi link NERDTreeBookmark normal
hi link NERDTreeHelp String
hi link NERDTreeHelpKey Identifier
hi link NERDTreeHelpCommand Identifier
hi link NERDTreeHelpTitle Macro
hi link NERDTreeToggleOn Question
hi link NERDTreeToggleOff WarningMsg
hi link NERDTreeLinkTarget Type
hi link NERDTreeLinkFile Macro
hi link NERDTreeLinkDir Macro
hi link NERDTreeDir Directory
hi link NERDTreeUp Directory
hi link NERDTreeFile Normal
hi link NERDTreeCWD Statement
hi link NERDTreeOpenable Title
hi link NERDTreeClosable Title
hi link NERDTreeIgnore ignore
hi link NERDTreeRO WarningMsg
hi link NERDTreeFlags Number
hi link phpConstant Constant
hi link phpCoreConstant Constant
hi link phpComment Comment
hi link phpDocTags PreProc
hi link phpDocCustomTags Type
hi link phpException Exception
hi link phpBoolean Boolean
hi link phpStorageClass StorageClass
hi link phpSCKeyword StorageClass
hi link phpFCKeyword Define
hi link phpStructure Structure
hi link phpStringSingle String
hi link phpStringDouble String
hi link phpBacktick String
hi link phpNumber Number
hi link phpFloat Float
hi link phpMethods Function
hi link phpFunctions Function
hi link phpBaselib Function
hi link phpRepeat Repeat
hi link phpConditional Conditional
hi link phpLabel Label
hi link phpStatement Statement
hi link phpKeyword Statement
hi link phpType Type
hi link phpInclude Include
hi link phpDefine Define
hi link phpBackslashSequences SpecialChar
hi link phpBackslashDoubleQuote SpecialChar
hi link phpBackslashSingleQuote SpecialChar
hi link phpParent Delimiter
hi link phpBrackets Delimiter
hi link phpIdentifierConst Delimiter
hi link phpParentError Error
hi link phpOctalError Error
hi link phpInterpSimpleError Error
hi link phpInterpBogusDollarCurley Error
hi link phpInterpDollarCurly1 Error
hi link phpInterpDollarCurly2 Error
hi link phpInterpSimpleBracketsInner String
hi link phpInterpSimpleCurly Delimiter
hi link phpInterpVarname Identifier
hi link phpTodo Todo
hi link phpDocTodo Todo
hi link phpMemberSelector Structure
hi link phpIntVar Identifier
hi link phpEnvVar Identifier
hi link phpOperator Operator
hi link phpVarSelector Operator
hi link phpRelation Operator
hi link phpIdentifier Identifier
hi link phpIdentifierSimply Identifier
hi link pythonStatement Statement
hi link pythonConditional Conditional
hi link pythonRepeat Repeat
hi link pythonOperator Operator
hi link pythonException Exception
hi link pythonInclude Include
hi link pythonDecorator Define
hi link pythonFunction Function
hi link pythonComment Comment
hi link pythonTodo Todo
hi link pythonString String
hi link pythonRawString String
hi link pythonQuotes String
hi link pythonTripleQuotes pythonQuotes
hi link pythonEscape Special
hi link pythonNumber Number
hi link pythonBuiltin Function
hi link pythonExceptions Structure
hi link pythonSpaceError Error
hi link pythonDoctest Special
hi link pythonDoctestValue Define
hi link rubyClass rubyDefine
hi link rubyModule rubyDefine
hi link rubyMethodExceptional rubyDefine
hi link rubyDefine Define
hi link rubyFunction Function
hi link rubyConditional Conditional
hi link rubyConditionalModifier rubyConditional
hi link rubyExceptional rubyConditional
hi link rubyRepeat Repeat
hi link rubyRepeatModifier rubyRepeat
hi link rubyOptionalDo rubyRepeat
hi link rubyControl Statement
hi link rubyInclude Include
hi link rubyInteger Number
hi link rubyASCIICode Character
hi link rubyFloat Float
hi link rubyBoolean Boolean
hi link rubyException Exception
hi link rubyIdentifier Identifier
hi link rubyClassVariable rubyIdentifier
hi link rubyConstant Type
hi link rubyGlobalVariable rubyIdentifier
hi link rubyBlockParameter rubyIdentifier
hi link rubyInstanceVariable rubyIdentifier
hi link rubyPredefinedIdentifier rubyIdentifier
hi link rubyPredefinedConstant rubyPredefinedIdentifier
hi link rubyPredefinedVariable rubyPredefinedIdentifier
hi link rubySymbol Constant
hi link rubyKeyword Keyword
hi link rubyOperator Operator
hi link rubyBeginEnd Statement
hi link rubyAccess Statement
hi link rubyAttribute Statement
hi link rubyEval Statement
hi link rubyPseudoVariable Constant
hi link rubyComment Comment
hi link rubyData Comment
hi link rubyDataDirective Delimiter
hi link rubyDocumentation Comment
hi link rubyTodo Todo
hi link rubyQuoteEscape rubyStringEscape
hi link rubyStringEscape Special
hi link rubyInterpolationDelimiter Delimiter
hi link rubyNoInterpolation rubyString
hi link rubySharpBang PreProc
hi link rubyRegexpDelimiter rubyStringDelimiter
hi link rubySymbolDelimiter rubyStringDelimiter
hi link rubyStringDelimiter Delimiter
hi link rubyHeredoc rubyString
hi link rubyString String
hi link rubyRegexpEscape rubyRegexpSpecial
hi link rubyRegexpQuantifier rubyRegexpSpecial
hi link rubyRegexpAnchor rubyRegexpSpecial
hi link rubyRegexpDot rubyRegexpCharClass
hi link rubyRegexpCharClass rubyRegexpSpecial
hi link rubyRegexpSpecial Special
hi link rubyRegexpComment Comment
hi link rubyRegexp rubyString
hi link rubyInvalidVariable Error
hi link rubyError Error
hi link rubySpaceError rubyError
hi link shArithRegion shShellVariables
hi link shAtExpr shSetList
hi link shBeginHere shRedir
hi link shCaseBar shConditional
hi link shCaseCommandSub shCommandSub
hi link shCaseDoubleQuote shDoubleQuote
hi link shCaseIn shConditional
hi link shQuote shOperator
hi link shCaseSingleQuote shSingleQuote
hi link shCaseStart shConditional
hi link shCmdSubRegion shShellVariables
hi link shColon shComment
hi link shDerefOp shOperator
hi link shDerefPOL shDerefOp
hi link shDerefPPS shDerefOp
hi link shDeref shShellVariables
hi link shDerefDelim shOperator
hi link shDerefSimple shDeref
hi link shDerefSpecial shDeref
hi link shDerefString shDoubleQuote
hi link shDerefVar shDeref
hi link shDoubleQuote shString
hi link shEcho shString
hi link shEchoDelim shOperator
hi link shEchoQuote shString
hi link shForPP shLoop
hi link shEmbeddedEcho shString
hi link shEscape shCommandSub
hi link shExDoubleQuote shDoubleQuote
hi link shExSingleQuote shSingleQuote
hi link shFunction Function
hi link shHereDoc shString
hi link shHerePayload shHereDoc
hi link shLoop shStatement
hi link shMoreSpecial shSpecial
hi link shOption shCommandSub
hi link shPattern shString
hi link shParen shArithmetic
hi link shPosnParm shShellVariables
hi link shQuickComment shComment
hi link shRange shOperator
hi link shRedir shOperator
hi link shSetListDelim shOperator
hi link shSetOption shOption
hi link shSingleQuote shString
hi link shSource shOperator
hi link shStringSpecial shSpecial
hi link shSubShRegion shOperator
hi link shTestOpr shConditional
hi link shTestPattern shString
hi link shTestDoubleQuote shString
hi link shTestSingleQuote shString
hi link shVariable shSetList
hi link shWrapLineOperator shOperator
hi link bashAdminStatement shStatement
hi link bashSpecialVariables shShellVariables
hi link bashStatement shStatement
hi link shFunctionParen Delimiter
hi link shFunctionDelim Delimiter
hi link kshSpecialVariables shShellVariables
hi link kshStatement shStatement
hi link shCaseError Error
hi link shCondError Error
hi link shCurlyError Error
hi link shDerefError Error
hi link shDerefOpError Error
hi link shDerefWordError Error
hi link shDoError Error
hi link shEsacError Error
hi link shIfError Error
hi link shInError Error
hi link shParenError Error
hi link shTestError Error
hi link shDTestError Error
hi link shArithmetic Special
hi link shCharClass Identifier
hi link shSnglCase Statement
hi link shCommandSub Special
hi link shComment Comment
hi link shConditional Conditional
hi link shCtrlSeq Special
hi link shExprRegion Delimiter
hi link shFunctionKey Function
hi link shFunctionName Function
hi link shNumber Number
hi link shOperator Operator
hi link shRepeat Repeat
hi link shSet Statement
hi link shSetList Identifier
hi link shShellVariables PreProc
hi link shSpecial Special
hi link shStatement Statement
hi link shString String
hi link shTodo Todo
hi link shAlias Identifier
hi link shHereDoc01 shRedir
hi link shHereDoc02 shRedir
hi link shHereDoc03 shRedir
hi link shHereDoc04 shRedir
hi link shHereDoc05 shRedir
hi link shHereDoc06 shRedir
hi link shHereDoc07 shRedir
hi link shHereDoc08 shRedir
hi link shHereDoc09 shRedir
hi link shHereDoc10 shRedir
hi link shHereDoc11 shRedir
hi link shHereDoc12 shRedir
hi link shHereDoc13 shRedir
hi link shHereDoc14 shRedir
hi link shHereDoc15 shRedir
hi link shHereDoc16 shRedir
hi link shHereDoc17 shRedir
hi link shHereDoc18 shRedir
hi link shHereDoc19 shRedir
hi link shHereDoc20 shRedir
hi link shHereDoc21 shRedir
hi link shHereDoc22 shRedir
hi link shHereDoc23 shRedir
hi link shHereDoc24 shRedir
hi link shHereDoc25 shRedir
hi link shHereDoc26 shRedir
hi link shHereDoc27 shRedir
hi link shHereDoc28 shRedir
hi link shHereDoc29 shRedir
hi link shHereDoc30 shRedir
hi link shHereDoc31 shRedir
hi link shHereDoc32 shRedir
hi link sshconfigConstant Constant
hi link sshconfigComment Comment
hi link sshconfigKeyword Keyword
hi sshconfigLogLevel guifg=#E65100 ctermfg=166 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi sshconfigMatch guifg=#F57C00 ctermfg=208 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link sshconfigYesNo Boolean
hi GitGutterAdd guifg=#33691E ctermfg=236 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi GitGutterChange guifg=#E65100 ctermfg=166 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi GitGutterDelete guifg=#B71C1C ctermfg=124 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi GitGutterChangeDelete guifg=#B71C1C ctermfg=124 guibg=#D7CCC8 ctermbg=252 gui=NONE cterm=NONE
hi link javaScriptEndColons Operator
hi link javaScriptOpSymbols Operator
hi link javaScriptLogicSymbols Boolean
hi link javaScriptParens Operator
hi link javaScriptTemplateDelim Operator
hi link javaScriptDocComment Comment
hi link javaScriptDocTags Special
hi link javaScriptDocSeeTag Function
hi link javaScriptDocParam Function
hi link javaScriptString String
hi link javaScriptTemplateString String
hi link javaScriptFloat Number
hi link javaScriptPrototype Type
hi link javaScriptSource Special
hi link javaScriptGlobalObjects Special
hi link javaScriptExceptions Special
hi link javaScriptParensErrA Error
hi link javaScriptParensErrB Error
hi link javaScriptParensErrC Error
hi link javaScriptDomErrNo Error
hi link javaScriptDomNodeConsts Constant
hi link javaScriptDomElemAttrs Label
hi link javaScriptDomElemFuncs Type
hi link javaScriptWebAPI Type
hi link javaScriptHtmlElemAttrs Label
hi link javaScriptHtmlElemFuncs Type
hi link javaScriptCssStyles Type
hi link javaScriptBrowserObjects Constant
hi link javaScriptDOMObjects Constant
hi link javaScriptDOMMethods Type
hi link javaScriptDOMProperties Label
hi link javaScriptAjaxObjects Constant
hi link javaScriptAjaxMethods Type
hi link javaScriptAjaxProperties Label
hi link javaScriptFuncKeyword Function
hi link javaScriptFuncDef PreProc
hi link javaScriptFuncExp Title
hi link javaScriptFuncArg Special
hi link javaScriptFuncComma Operator
hi link javaScriptFuncEq Operator
hi link javaScriptHtmlEvents Constant
hi link javaScriptHtmlElemProperties Label
hi link javaScriptEventListenerKeywords Type
hi link javaScriptPropietaryObjects Constant
hi link plug1 Title
hi link plug2 Repeat
hi link plugH2 Type
hi link plugX Exception
hi link plugBracket Structure
hi link plugNumber Number
hi link plugDash Special
hi link plugPlus Constant
hi link plugStar Boolean
hi link plugMessage Function
hi link plugName Label
hi link plugInstall Function
hi link plugUpdate Type
hi link plugError Error
hi link plugRelDate Comment
hi link plugEdge PreProc
hi link plugSha Identifier
hi link plugTag Constant
hi link plugNotLoaded Comment
hi link viminfoComment Comment
hi link viminfoError Error
hi link viminfoStatement Statement
hi link xmlTodo Todo
hi link xmlTag Function
hi link xmlTagName Function
hi link xmlEndTag Identifier
hi link xmlNamespace Tag
hi link xmlEntity Statement
hi link xmlEntityPunct Type
hi link xmlAttribPunct Comment
hi link xmlAttrib Type
hi link xmlString String
hi link xmlComment Comment
hi link xmlCommentStart xmlComment
hi link xmlCommentPart Comment
hi link xmlCommentError Error
hi link xmlError Error
hi link xmlProcessingDelim Comment
hi link xmlProcessing Type
hi link xmlCdata String
hi link xmlCdataCdata Statement
hi link xmlCdataStart Type
hi link xmlCdataEnd Type
hi link xmlDocTypeDecl Function
hi link xmlDocTypeKeyword Statement
hi link xmlInlineDTD Function
hi link yamlTodo Todo
hi link yamlComment Comment
hi link yamlDocumentStart PreProc
hi link yamlDocumentEnd PreProc
hi link yamlDirectiveName Keyword
hi link yamlTAGDirective yamlDirectiveName
hi link yamlTagHandle String
hi link yamlTagPrefix String
hi link yamlYAMLDirective yamlDirectiveName
hi link yamlReservedDirective Error
hi link yamlYAMLVersion Number
hi link yamlString String
hi link yamlFlowString yamlString
hi link yamlFlowStringDelimiter yamlString
hi link yamlEscape SpecialChar
hi link yamlSingleEscape SpecialChar
hi link yamlBlockCollectionItemStart Label
hi link yamlBlockMappingKey Identifier
hi link yamlBlockMappingMerge Special
hi link yamlFlowMappingKey Identifier
hi link yamlFlowMappingMerge Special
hi link yamlMappingKeyStart Special
hi link yamlFlowIndicator Special
hi link yamlKeyValueDelimiter Special
hi link yamlConstant Constant
hi link yamlNull yamlConstant
hi link yamlBool yamlConstant
hi link yamlAnchor Type
hi link yamlAlias Type
hi link yamlNodeTag Type
hi link yamlInteger Number
hi link yamlFloat Float
hi link yamlTimestamp Number


" ===================================
" Generated by Estilo 1.4.1
" https://github.com/jacoborus/estilo
" ===================================
