"=============================================================================
" PLUGINS:
"=============================================================================
let s:vim_dir = '~/.vim/'
let s:vimplug_path = s:vim_dir . '/autoload/plug.vim'
if empty(glob(s:vimplug_path))
  silent execute '!curl -fLo '.s:vimplug_path.' --create-dirs '.
    \'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | close | source $MYVIMRC | AirlineRefresh
endif

call plug#begin(s:vim_dir . 'packages')

" COLORSCHEME PLUGINS: -------------------------------------------------------
Plug 'boweevil/vim-gruvbox8', {'branch': 'boweevil'}

" IDE PLUGINS: ---------------------------------------------------------------
Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-ctrlspace/vim-ctrlspace'
Plug 'tpope/vim-fugitive'
Plug 'voldikss/vim-floaterm'
Plug 'junegunn/vim-peekaboo'
Plug 'unblevable/quick-scope'
Plug 'jessarcher/vim-sayonara', { 'on': 'Sayonara' }
Plug 'terryma/vim-smooth-scroll'
Plug 'vim-test/vim-test'

" FORMATTING PLUGINS: --------------------------------------------------------
Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
  Plug 'tpope/vim-repeat'
Plug 'tomtom/tcomment_vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'wellle/targets.vim'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'nelstrom/vim-visual-star-search'

" SYNTAX PLUGINS: ------------------------------------------------------------
" polyglot_disabled must be set before vim-polyglot is loaded
let g:polyglot_disabled = ['dockerfile']
Plug 'sheerun/vim-polyglot'
Plug 'cappyzawa/starlark.vim'

" VIMSCRIPT PLUGINS: -------------------------------------------------------
Plug 'lifepillar/vim-colortemplate'

" PYTHON PLUGINS: ------------------------------------------------------------
Plug 'vim-python/python-syntax', { 'for': ['python'] }
Plug 'psf/black', { 'tag': '*' }
Plug 'nvie/vim-flake8', { 'for': ['python'] }

" GOLANG PLUGINS: ------------------------------------------------------------
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" MARKDOWN PLUGINS: ----------------------------------------------------------
Plug 'gabrielelana/vim-markdown'

" OTHER PLUGINS: -------------------------------------------------------------
Plug 'gerw/vim-HiLinkTrace'
Plug 'vim-scripts/VisIncr'
Plug 'ekalinin/Dockerfile.vim'

call plug#end()

"=============================================================================
" CONFIGURATION:
"=============================================================================

" COLORSCHEME PLUGINS: -------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/colors/gruvbox8.vim'

" IDE PLUGINS: ---------------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/vim-airline.vim'
execute 'source' s:vim_dir . 'config.d/vim-gitgutter.vim'
execute 'source' s:vim_dir . 'config.d/ctrlp.vim'
execute 'source' s:vim_dir . 'config.d/ctrlspace.vim'
execute 'source' s:vim_dir . 'config.d/vim-fugitive.vim'
execute 'source' s:vim_dir . 'config.d/floaterm.vim'
execute 'source' s:vim_dir . 'config.d/quickscope.vim'
execute 'source' s:vim_dir . 'config.d/sayonara.vim'
execute 'source' s:vim_dir . 'config.d/smooth-scroll.vim'
execute 'source' s:vim_dir . 'config.d/vim-test.vim'

" FORMATTING PLUGINS: --------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/ultisnips.vim'
execute 'source' s:vim_dir . 'config.d/splitjoin.vim'

" PYTHON PLUGINS: ------------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/python.vim'
execute 'source' s:vim_dir . 'config.d/python-syntax.vim'
execute 'source' s:vim_dir . 'config.d/black.vim'

" GOLANG PLUGINS: ------------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/golang.vim'
execute 'source' s:vim_dir . 'config.d/vim-go.vim'

" MARKDOWN PLUGINS: ----------------------------------------------------------
execute 'source' s:vim_dir . 'config.d/markdown-preview.vim'

" GENERAL CONFIGURATION: -----------------------------------------------------
execute 'source' s:vim_dir . 'config.d/general-configuration.vim'
execute 'source' s:vim_dir . 'config.d/key-bindings.vim'
execute 'source' s:vim_dir . 'config.d/functions.vim'
execute 'source' s:vim_dir . 'config.d/file-types.vim'

