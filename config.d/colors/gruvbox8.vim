let g:gruvbox_bold = 1
let g:gruvbox_italics = 1
let g:gruvbox_italicize_strings = 1
let g:gruvbox_filetype_hi_groups = 1
let g:gruvbox_plugin_hi_groups = 1
let g:gruvbox_transp_bg = 0

if (has("termguicolors"))
  set termguicolors
endif

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  set background=dark
  colorscheme gruvbox8
  let g:airline_theme='gruvbox8'
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
