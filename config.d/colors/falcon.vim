let g:falcon_airline=1
let g:falcon_background = 1
let g:falcon_inactive = 1

if (has("termguicolors"))
  set termguicolors
endif

let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  set background=dark
  colorscheme falcon
  let g:airline_theme='falcon'
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
