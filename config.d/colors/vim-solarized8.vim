" Use truecolor
if (has("termguicolors"))
  set termguicolors
endif

let g:solarized_visibility='normal'
let g:solarized_diffmode='high'
let g:solarized_termtrans=0
let g:solarized_statusline='high'
let g:solarized_italics=1
let g:solarized_old_cursor_style=0
let g:solarized_use16=0
let g:solarized_extra_hi_groups=1
let g:ale_sign_highlight_linenrs=1

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  source ~/.vimrc_background
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry

function! Solar_swap()
  if &background ==? 'dark'
    set background=light
    execute 'silent !'.shellescape(expand('~/bin/SolarSwap.sh')).' light'
  else
    set background=dark
    execute 'silent !'.shellescape(expand('~/bin/SolarSwap.sh')).' dark'
  endif
endfunction

command! SolarSwap call Solar_swap()
