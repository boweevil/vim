function! SetBackgroundMode(...)
    let s:new_bg = "dark"
    if has('macunix')
      let s:mode = systemlist("defaults read -g AppleInterfaceStyle")[0]
    else
      let s:mode = "dark"
    endif
    if s:mode ==? "dark"
        let s:new_bg = "dark"
    else
        let s:new_bg = "light"
    endif
    if &background !=? s:new_bg
        let &background = s:new_bg
    endif
endfunction
call SetBackgroundMode()
call timer_start(3000, "SetBackgroundMode", {"repeat": -1})
