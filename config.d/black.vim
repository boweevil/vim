let g:black_fast=0
let g:black_linelength=88
let g:black_skip_string_normalization=0
let g:black_virtualenv='~/.local/share/nvim/black'

if filereadable('.black.toml')
  autocmd BufWritePre *.py execute ':Black --config .black.toml'
else
  autocmd BufWritePre *.py execute ':Black'
endif
