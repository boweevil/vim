let g:go_def_mode = 'gopls'
let g:go_referrers_mode = 'gopls'
let g:go_info_mode = 'gopls'
let g:go_implements_mode = 'gopls'

let g:go_def_mapping_enabled = 0

" Deprecated. See 'g:go_diagnostics_level'.
" let g:go_diagnostics_enabled = 0
let g:go_diagnostics_level = 2

" Use popups instead of window for docs (Shift + K)
let g:go_doc_popup_window = 1

" Add tags
autocmd FileType go nmap gtj :GoAddTags json<cr>
autocmd FileType go nmap gty :GoAddTags yaml<cr>
autocmd FileType go nmap gtx :GoRemoveTags<cr>
