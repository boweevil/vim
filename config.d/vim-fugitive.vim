autocmd FileType gitcommit setlocal spell

nmap <leader>gs :Git<cr>
nmap <leader>gd :Git diff<cr>
nmap <leader>gc :Git commit<cr>
nmap <leader>gb :Git blame<cr>
nmap <leader>gl :Git log<cr>
nmap <leader>gp :Git push<cr>
nmap <leader>gw :Git write<cr>
