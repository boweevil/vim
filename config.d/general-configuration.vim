syntax on

scriptencoding utf-8

" Don't act like Vi.
setlocal nocompatible

set ruler
set showcmd
set number

" Tell vim where to place splits.
set splitbelow
set splitright

" Tabs
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

" Line Breaks
set linebreak

" Don't close buffers which are not visible.
set hidden

" Searching
set incsearch
set hlsearch

" Fix backspace behavior.
set backspace=indent,eol,start

" Fuzzy file finder
" Search for files recursively from working directory when using :find.
set path+=**

" Wild mode
" Display a nice menu when completing file names.
set wildmode=longest:full,full
set wildmenu

" Show trailing blank space.
set listchars=tab:│\ ,trail:·
set list

" Show cursor line and column
set nocursorline
set nocursorcolumn

" Show 2 columns for signs
set signcolumn=yes

" Scrolloff settings.  When to start scrolling.
set scrolloff=8
set sidescrolloff=8

filetype plugin indent on
filetype plugin on
set omnifunc=syntaxcomplete#Complete
