set encoding=UTF-8

if has('macunix')
  set guifont=JetBrainsMono_Nerd_Font_Mono:h13
elseif has('unix')
  set guifont=JetBrainsMono\ Nerd\ Font\ Mono\ 13
endif
