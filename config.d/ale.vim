" Set to 1 to keep the gutter always open.
let g:ale_sign_column_always = 0

" Set the signs for indication of errors and warnings.
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

" Set this to 0 to disable highlighting
let g:ale_set_highlights = 1
if has('nvim')
  let g:ale_sign_highlight_linenrs = 1
endif

" Show errors in airline.
let g:airline#extensions#ale#enabled = 1

let g:ale_open_list = 0
let g:ale_keep_list_window_open = 0

" Show which linter is complaining
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %code%: %s [%severity%]'

let g:ale_linters = {
\   'css': ['csslint'],
\   'python': ['pylint'],
\}

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
